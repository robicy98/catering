package edu.bbte.idde.srim1761.backend.dao.util;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

@Configuration
public class EntityManaggerFactoryBean {

    @Bean
    @Qualifier("jpa")
    public EntityManager getEntityManagerJpa() {
        return Persistence.createEntityManagerFactory("menuPU").createEntityManager();
    }
}
