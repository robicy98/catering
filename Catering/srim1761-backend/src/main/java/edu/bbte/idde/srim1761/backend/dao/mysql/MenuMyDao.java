package edu.bbte.idde.srim1761.backend.dao.mysql;

import edu.bbte.idde.srim1761.backend.dao.MenuDao;
import edu.bbte.idde.srim1761.backend.dao.RepositoryException;
import edu.bbte.idde.srim1761.backend.dao.jdbc.ConnectionPool;
import edu.bbte.idde.srim1761.backend.model.Menu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MenuMyDao extends MyDao<Menu> implements MenuDao {
    private static final Logger LOG = LoggerFactory.getLogger(MenuMyDao.class);
    private static final String SQL_ERROR = "SQL execution failed!";

    @Autowired
    private ConnectionPool connectionPool;

    @Override
    public List<Menu> findAll() {

        Connection connection = null;
        List<Menu> results = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            Statement state =  connection.createStatement();
            String query = "SELECT id, eloetel, leves, foetel, desszert, ital FROM Menu;";
            ResultSet resultSet = state.executeQuery(query);
            while (resultSet.next()) {
                results.add(
                        new Menu(resultSet.getInt(1),
                                resultSet.getString(2),
                                resultSet.getString(3),
                                resultSet.getString(4),
                                resultSet.getString(5),
                                resultSet.getString(6)
                ));
            }
        } catch (SQLException e) {
            LOG.info(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            connectionPool.returnConnection(connection);
        }
        return results;
    }

    @Override
    public Menu findById(int id) {
        Connection connection = null;
        Menu result = null;
        try {
            connection = connectionPool.getConnection();
            String query = "SELECT id, eloetel, leves, foetel, desszert, ital FROM Menu where id = ?";
            PreparedStatement state =  connection.prepareStatement(query);
            state.setInt(1,id);
            ResultSet resultSet = state.executeQuery(query);
            if (resultSet.next()) {
                result = new Menu();
                result.setId(resultSet.getInt("id"));
                result.setEloetel(resultSet.getString("eloetel"));
                result.setLeves(resultSet.getString("leves"));
                result.setFoetel(resultSet.getString("foetel"));
                result.setDesszert(resultSet.getString("desszert"));
                result.setItal(resultSet.getString("ital"));
            }
        } catch (SQLException e) {
            LOG.error(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            if (connection != null) {
                connectionPool.returnConnection(connection);
            }
        }
        return result;
    }

    @Override
    public void create(Menu entity) {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            String query = "insert into Menu (eloetel, leves, foetel, desszert, ital) values (?,?,?,?,?);";
            PreparedStatement state = connection.prepareStatement(query);
            state.setString(1,entity.getEloetel());
            state.setString(2,entity.getLeves());
            state.setString(3,entity.getFoetel());
            state.setString(4,entity.getDesszert());
            state.setString(5,entity.getItal());
            state.executeUpdate();
        } catch (SQLException e) {
            LOG.error(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            if (connection != null) {
                connectionPool.returnConnection(connection);
            }
        }
    }

    @Override
    public void update(int id, Menu entity) {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            String query = "update Menu set eloetel=?,leves=?,foetel=?,desszert=?,ital=? where id = ?;";
            PreparedStatement state = connection.prepareStatement(query);
            state.setString(1,entity.getEloetel());
            state.setString(2,entity.getLeves());
            state.setString(3,entity.getFoetel());
            state.setString(4,entity.getDesszert());
            state.setString(5,entity.getItal());
            state.setInt(6,entity.getId());
        } catch (SQLException e) {
            LOG.error(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            if (connection != null) {
                connectionPool.returnConnection(connection);
            }
        }
    }

    @Override
    public void delete(int id) {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            String query = "delete from Menu where id = ?;";
            PreparedStatement state = connection.prepareStatement(query);
            state.setInt(1,id);
        } catch (SQLException e) {
            LOG.error(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            if (connection != null) {
                connectionPool.returnConnection(connection);
            }
        }
    }

    @Override
    public List<Menu> findByEloetel(String parameter) {
        Connection connection = null;
        List<Menu> results = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            String query = "SELECT id, eloetel, leves, foetel, desszert, ital FROM Menu where eloetel = ?;";
            PreparedStatement state =  connection.prepareStatement(query);
            state.setString(1,parameter);
            ResultSet resultSet = state.executeQuery(query);
            while (resultSet.next()) {
                results.add(
                        new Menu(resultSet.getInt(1),
                                resultSet.getString(2),
                                resultSet.getString(3),
                                resultSet.getString(4),
                                resultSet.getString(5),
                                resultSet.getString(6)
                        ));
            }
        } catch (SQLException e) {
            LOG.error(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            connectionPool.returnConnection(connection);
        }
        return results;
    }

    @Override
    public List<Menu> findByLeves(String parameter) {
        Connection connection = null;
        List<Menu> results = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            String query = "SELECT id, eloetel, leves, foetel, desszert, ital FROM Menu where leves = ?;";
            PreparedStatement state =  connection.prepareStatement(query);
            state.setString(1,parameter);
            ResultSet resultSet = state.executeQuery(query);
            while (resultSet.next()) {
                results.add(
                        new Menu(resultSet.getInt(1),
                                resultSet.getString(2),
                                resultSet.getString(3),
                                resultSet.getString(4),
                                resultSet.getString(5),
                                resultSet.getString(6)
                        ));
            }
        } catch (SQLException e) {
            LOG.error(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            connectionPool.returnConnection(connection);
        }
        return results;
    }

    @Override
    public List<Menu> findByFoetel(String parameter) {
        Connection connection = null;
        List<Menu> results = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            String query = "SELECT id, eloetel, leves, foetel, desszert, ital FROM Menu where foetel = ?;";
            PreparedStatement state =  connection.prepareStatement(query);
            state.setString(1,parameter);
            ResultSet resultSet = state.executeQuery(query);
            while (resultSet.next()) {
                results.add(
                        new Menu(resultSet.getInt(1),
                                resultSet.getString(2),
                                resultSet.getString(3),
                                resultSet.getString(4),
                                resultSet.getString(5),
                                resultSet.getString(6)
                        ));
            }
        } catch (SQLException e) {
            LOG.error(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            connectionPool.returnConnection(connection);
        }
        return results;
    }

    @Override
    public List<Menu> findByDesszert(String parameter) {
        Connection connection = null;
        List<Menu> results = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            String query = "SELECT id, eloetel, leves, foetel, desszert, ital FROM Menu where desszert = ?;";
            PreparedStatement state =  connection.prepareStatement(query);
            state.setString(1,parameter);
            ResultSet resultSet = state.executeQuery(query);
            while (resultSet.next()) {
                results.add(
                        new Menu(resultSet.getInt(1),
                                resultSet.getString(2),
                                resultSet.getString(3),
                                resultSet.getString(4),
                                resultSet.getString(5),
                                resultSet.getString(6)
                        ));
            }
        } catch (SQLException e) {
            LOG.error(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            connectionPool.returnConnection(connection);
        }
        return results;
    }

    @Override
    public List<Menu> findByItal(String parameter) {
        Connection connection = null;
        List<Menu> results = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            String query = "SELECT id, eloetel, leves, foetel, desszert, ital FROM Menu where ital = ?;";
            PreparedStatement state =  connection.prepareStatement(query);
            state.setString(1,parameter);
            ResultSet resultSet = state.executeQuery(query);
            while (resultSet.next()) {
                results.add(
                        new Menu(resultSet.getInt(1),
                                resultSet.getString(2),
                                resultSet.getString(3),
                                resultSet.getString(4),
                                resultSet.getString(5),
                                resultSet.getString(6)
                        ));
            }
        } catch (SQLException e) {
            LOG.error(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            connectionPool.returnConnection(connection);
        }
        return results;
    }
}