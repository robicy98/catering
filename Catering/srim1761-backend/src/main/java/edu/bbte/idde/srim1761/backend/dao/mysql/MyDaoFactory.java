package edu.bbte.idde.srim1761.backend.dao.mysql;

import edu.bbte.idde.srim1761.backend.dao.DaoFactory;
import edu.bbte.idde.srim1761.backend.dao.MenuDao;
import edu.bbte.idde.srim1761.backend.dao.UserDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("jdbc")
public class MyDaoFactory extends DaoFactory {

    @Override
    @Bean
    public MenuDao getMenuDao() {
        return new MenuMyDao();
    }

    @Override
    @Bean
    public UserDao getUserDao() {
        return new UserMyDao();
    }
}
