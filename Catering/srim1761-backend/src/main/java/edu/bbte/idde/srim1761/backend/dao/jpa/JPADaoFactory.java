package edu.bbte.idde.srim1761.backend.dao.jpa;

import edu.bbte.idde.srim1761.backend.dao.DaoFactory;
import edu.bbte.idde.srim1761.backend.dao.MenuDao;
import edu.bbte.idde.srim1761.backend.dao.UserDao;
import org.springframework.context.annotation.*;

@Configuration
@Profile("jpa")
public class JPADaoFactory extends DaoFactory {

    @Override
    @Bean
    public MenuDao getMenuDao() {
        return new MenuJPADao();
    }

    @Override
    @Bean
    public UserDao getUserDao() {
        return new UserJPADao();
    }
}
