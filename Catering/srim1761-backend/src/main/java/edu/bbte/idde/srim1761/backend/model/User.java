package edu.bbte.idde.srim1761.backend.model;

import javax.persistence.*;

@Entity
@Table(name = "User")
@NamedQueries({
        @NamedQuery(name = User.FIND_ALL, query = "from User"),
        @NamedQuery(name = User.FIND_BY_FIRSTNAME, query = "from User where firstname=:firstname"),
        @NamedQuery(name = User.FIND_BY_LASTNAME, query = "from User where lastname=:lastname"),
        @NamedQuery(name = User.FIND_BY_EMAIL, query = "from User where email=:email")
})
public class User extends BaseEntity {

    public static final String FIND_ALL = "User.findAll";
    public static final String FIND_BY_FIRSTNAME = "User.findByFirstname";
    public static final String FIND_BY_LASTNAME = "User.findByLastname";
    public static final String FIND_BY_EMAIL = "User.findByEmail";

    @Column(nullable = false, name = "firstname")
    private String firstname;
    @Column(nullable = false, name = "lastname")
    private String lastname;
    @Column(nullable = false, name = "email")
    private String email;

    public User() {
        super();
    }

    public User(int id, String firstname, String lastname, String email) {
        super(id);
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLasttname(String lasttname) {
        this.lastname = lasttname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
