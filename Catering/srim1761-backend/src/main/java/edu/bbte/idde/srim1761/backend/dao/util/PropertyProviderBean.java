package edu.bbte.idde.srim1761.backend.dao.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.PropertyResolver;

@Configuration
@PropertySource("classpath:/application.properties")
public class PropertyProviderBean {

    @Autowired
    private PropertyResolver resolver;

    public PropertyResolver getResolver() {
        return resolver;
    }

    public String getProperty(String key) {
        return resolver.getProperty(key);
    }

    public String getProperty(String key, String defaultValue) {
        return resolver.getProperty(key, defaultValue);
    }
}
