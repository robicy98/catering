package edu.bbte.idde.srim1761.backend.dao.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Deque;
import java.util.LinkedList;

import edu.bbte.idde.srim1761.backend.dao.RepositoryException;
import edu.bbte.idde.srim1761.backend.dao.util.PropertyProviderBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;

@Configuration
public final class ConnectionPool {
    private static final Logger LOG = LoggerFactory.getLogger(ConnectionPool.class);
    private Integer poolSize;

    private static ConnectionPool instance;
    private final Deque<Connection> pool = new LinkedList<>();

    @Autowired
    private PropertyProviderBean propertyProviderBean;

    private ConnectionPool() {
        try {
            Class.forName(propertyProviderBean.getProperty("DB_DRIVER"));
            String connectionUrl = "jdbc:" + propertyProviderBean.getProperty("DB_URI");
            poolSize = Integer.valueOf(propertyProviderBean.getProperty("DB_POOL_SIZE"));
            for (int i = 0; i < poolSize; ++i) {
                pool.push(DriverManager.getConnection(connectionUrl,
                            propertyProviderBean.getProperty("DB_USER"),
                            propertyProviderBean.getProperty("DB_PASSWORD")));
            }
            LOG.info("Initialized pool of size {}", poolSize);
        } catch (ClassNotFoundException | SQLException e) {
            LOG.error("Connection could not be established", e);
            throw new RepositoryException("Connection could not be established", e);
        }
    }

    public static void createTables(Connection connection) throws SQLException {
        String menuTable =
                " CREATE TABLE IF NOT EXISTS`Menu` (" +
                        "`id` INT NOT NULL AUTO_INCREMENT," +
                        "`eloetel` VARCHAR(100) NOT NULL," +
                        "`leves` VARCHAR(100) NOT NULL," +
                        "`foetel` VARCHAR(100) NOT NULL," +
                        "`desszert` VARCHAR(100) NOT NULL," +
                        "`ital` VARCHAR(100) NOT NULL," +
                        " PRIMARY KEY (`id`)" +
                        ");";
        String userTable =
                "CREATE TABLE IF NOT EXISTS `Users` (" +
                    "`id` INT NOT NULL AUTO_INCREMENT, " +
                    "`firstname` VARCHAR(100) NOT NULL DEFAULT ''," +
                    "`lastname` VARCHAR(100) NOT NULL DEFAULT ''," +
                    "`email` VARCHAR(100) NOT NULL DEFAULT ''," +
                    "PRIMARY KEY (`id`)" +
                    ");";
        Statement statement = connection.createStatement();
        statement.addBatch(menuTable);
        statement.addBatch(userTable);
        statement.executeBatch();
    }

    public synchronized Connection getConnection() {
        if (pool.isEmpty()) {
            throw new RepositoryException("No connections in pool");
        }
        LOG.info("Giving out connection from pool");
        return pool.pop();
    }

    public synchronized void returnConnection(Connection connection) {
        if (pool.size() < poolSize) {
            LOG.info("Returning connection to pool");
            pool.push(connection);
        }
    }

    public static synchronized ConnectionPool getInstance() {
        if (instance == null) {
            instance = new ConnectionPool();
        }
        return instance;
    }
}
