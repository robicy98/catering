package edu.bbte.idde.srim1761.backend.dao.jpa;

import edu.bbte.idde.srim1761.backend.dao.UserDao;
import edu.bbte.idde.srim1761.backend.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.persistence.EntityManager;
import java.util.List;

public class UserJPADao extends JPADao<User> implements UserDao {
    
    @Autowired
    @Qualifier("jpa")
    private EntityManager entityManager;
    
    @Override
    public List<User> finedByFirstname(String parameter) {
        return entityManager.createNamedQuery(User.FIND_BY_FIRSTNAME,User.class)
                .setParameter("firstname",parameter).getResultList();
    }

    @Override
    public List<User> finedByLastname(String parameter) {
        return entityManager.createNamedQuery(User.FIND_BY_LASTNAME,User.class)
                .setParameter("lastname",parameter).getResultList();
    }

    @Override
    public List<User> finedByEmail(String parameter) {
        return entityManager.createNamedQuery(User.FIND_BY_EMAIL,User.class)
                .setParameter("email",parameter).getResultList();
    }

    @Override
    public List<User> findAll() {
        return entityManager.createNamedQuery(User.FIND_ALL, User.class)
                .getResultList();
    }

    @Override
    public User findById(int id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public void delete(int id) {
        User user = entityManager.find(User.class,id);
        if (user != null) {
            entityManager.getTransaction().begin();
            entityManager.remove(user);
            entityManager.getTransaction().commit();
        }
    }
}
