package edu.bbte.idde.srim1761.backend.dao.jpa;

import edu.bbte.idde.srim1761.backend.dao.MenuDao;
import edu.bbte.idde.srim1761.backend.dao.mysql.MenuMyDao;
import edu.bbte.idde.srim1761.backend.model.Menu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.*;

import javax.persistence.EntityManager;
import java.util.List;

public class MenuJPADao extends JPADao<Menu> implements MenuDao {
    private static final Logger LOG = LoggerFactory.getLogger(MenuMyDao.class);

    @Autowired
    @Qualifier("jpa")
    private EntityManager entityManager;

    @Override
    public List<Menu> findByEloetel(String parameter) {
        return entityManager.createNamedQuery(Menu.FIND_BY_ELOETEL, Menu.class)
            .setParameter("eloetel", parameter).getResultList();
    }

    @Override
    public List<Menu> findByLeves(String parameter) {
        return entityManager.createNamedQuery(Menu.FIND_BY_LEVES, Menu.class)
                .setParameter("leves", parameter).getResultList();
    }

    @Override
    public List<Menu> findByFoetel(String parameter) {
        return entityManager.createNamedQuery(Menu.FIND_BY_FOETEL, Menu.class)
                .setParameter("foetel", parameter).getResultList();
    }

    @Override
    public List<Menu> findByDesszert(String parameter) {
        return entityManager.createNamedQuery(Menu.FIND_BY_DESSZERT, Menu.class)
                .setParameter("desszert", parameter).getResultList();
    }

    @Override
    public List<Menu> findByItal(String parameter) {
        return entityManager.createNamedQuery(Menu.FIND_BY_ITAL, Menu.class)
                .setParameter("ital", parameter).getResultList();
    }

    @Override
    public List<Menu> findAll() {
        LOG.info("FINDALL in Menu JPA");
        return entityManager.createNamedQuery(Menu.FIND_ALL, Menu.class).getResultList();
    }

    @Override
    public Menu findById(int id) {
        return entityManager.find(Menu.class, id);
    }

    @Override
    public void delete(int id) {
        Menu menu = entityManager.find(Menu.class,id);
        if (menu != null) {
            entityManager.getTransaction().begin();
            entityManager.remove(menu);
            entityManager.getTransaction().commit();
        }
    }
}
