package edu.bbte.idde.srim1761.backend.dao;

import edu.bbte.idde.srim1761.backend.dao.jpa.JPADaoFactory;
import edu.bbte.idde.srim1761.backend.dao.mysql.MyDaoFactory;
import edu.bbte.idde.srim1761.backend.dao.util.PropertyProvider;

public abstract class DaoFactory {
    private static DaoFactory instance;

    private static String daoType = "jpa";

    public static synchronized DaoFactory getInstance() {
        if (instance == null) {

            daoType = PropertyProvider.getProperty("daoType");

            if ("jdbc".equals(daoType)) {
                instance = new MyDaoFactory();
            } else {
                instance = new JPADaoFactory();
            }
        }
        return instance;
    }

    public abstract MenuDao getMenuDao();

    public abstract UserDao getUserDao();
}
