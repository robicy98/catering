package edu.bbte.idde.srim1761.backend.model;

import javax.persistence.*;

@Entity
@Table(name = "Menu")
@NamedQueries({
        @NamedQuery(name = Menu.FIND_ALL, query = "from Menu"),
        @NamedQuery(name = Menu.FIND_BY_ELOETEL, query = "from Menu where eloetel=:eloetel"),
        @NamedQuery(name = Menu.FIND_BY_LEVES, query = "from Menu where leves=:leves"),
        @NamedQuery(name = Menu.FIND_BY_FOETEL, query = "from Menu where foetel=:foetel"),
        @NamedQuery(name = Menu.FIND_BY_DESSZERT, query = "from Menu where desszert=:desszert"),
        @NamedQuery(name = Menu.FIND_BY_ITAL, query = "from Menu where ital=:ital")
})
public class Menu extends BaseEntity {

    public static final String FIND_ALL = "Menu.findAll";
    public static final String FIND_BY_ELOETEL = "Menu.findByEloetel";
    public static final String FIND_BY_LEVES = "Menu.findByLeves";
    public static final String FIND_BY_FOETEL = "Menu.findByFoetel";
    public static final String FIND_BY_DESSZERT = "Menu.findByDesszert";
    public static final String FIND_BY_ITAL = "Menu.findByItal";

    @Column(nullable = false, name = "eloetel")
    private String eloetel;
    @Column(nullable = false, name = "leves")
    private String leves;
    @Column(nullable = false, name = "foetel")
    private String foetel;
    @Column(nullable = false, name = "desszert")
    private String desszert;
    @Column(nullable = false, name = "ital")
    private String ital;

    public Menu() {
        super();
    }

    public Menu(int id, String eloetel, String leves, String foetel, String desszert, String ital) {
        super(id);
        this.eloetel = eloetel;
        this.leves = leves;
        this.foetel = foetel;
        this.desszert = desszert;
        this.ital = ital;
    }

    public Menu(String eloetel, String leves, String foetel, String desszert, String ital) {
        super();
        this.eloetel = eloetel;
        this.leves = leves;
        this.foetel = foetel;
        this.desszert = desszert;
        this.ital = ital;
    }

    public String getEloetel() {
        return eloetel;
    }

    public void setEloetel(String eloetel) {
        this.eloetel = eloetel;
    }

    public String getLeves() {
        return leves;
    }

    public void setLeves(String leves) {
        this.leves = leves;
    }

    public String getFoetel() {
        return foetel;
    }

    public void setFoetel(String foetel) {
        this.foetel = foetel;
    }

    public String getDesszert() {
        return desszert;
    }

    public void setDesszert(String desszert) {
        this.desszert = desszert;
    }

    public String getItal() {
        return ital;
    }

    public void setItal(String ital) {
        this.ital = ital;
    }

    @Override
    public String toString() {
        return "Menu[" +
                "eloetel='" + eloetel + '\'' +
                ", leves='" + leves + '\'' +
                ", foetel='" + foetel + '\'' +
                ", desszert='" + desszert + '\'' +
                ", ital='" + ital + '\'' +
                ']';
    }
}
