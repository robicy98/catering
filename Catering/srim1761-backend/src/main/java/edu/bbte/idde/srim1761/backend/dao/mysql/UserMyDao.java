package edu.bbte.idde.srim1761.backend.dao.mysql;

import edu.bbte.idde.srim1761.backend.dao.RepositoryException;
import edu.bbte.idde.srim1761.backend.dao.UserDao;
import edu.bbte.idde.srim1761.backend.dao.jdbc.ConnectionPool;
import edu.bbte.idde.srim1761.backend.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserMyDao extends MyDao<User> implements UserDao {
    private static final Logger LOG = LoggerFactory.getLogger(MenuMyDao.class);
    private static final String SQL_ERROR = "SQL execution failed!";

    @Autowired
    private ConnectionPool connectionPool;

    @Override
    public List<User> findAll() {

        Connection connection = null;
        List<User> results = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            Statement state =  connection.createStatement();
            String query = "SELECT id, firstname, lastname, email FROM Users;";
            ResultSet resultSet = state.executeQuery(query);
            while (resultSet.next()) {
                results.add(new User(
                                resultSet.getInt(1),
                                resultSet.getString(2),
                                resultSet.getString(3),
                                resultSet.getString(4)));
            }
        } catch (SQLException e) {
            LOG.error(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            if (connection != null) {
                connectionPool.returnConnection(connection);
            }
        }
        return results;
    }

    @Override
    public User findById(int id) {
        Connection connection = null;
        User results = new User();
        try {
            connection = connectionPool.getConnection();
            String query = "SELECT id, firstname, lastname, email FROM Users where id = ?";
            PreparedStatement state =  connection.prepareStatement(query);
            state.setInt(1,id);
            ResultSet resultSet = state.executeQuery();
            if (resultSet.next()) {
                results.setId(resultSet.getInt("id"));
                results.setFirstname(resultSet.getString("firstname"));
                results.setLasttname(resultSet.getString("lasttname"));
                results.setEmail(resultSet.getString("email"));
            }
        } catch (SQLException e) {
            LOG.error(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            if (connection != null) {
                connectionPool.returnConnection(connection);
            }
        }
        return results;
    }

    @Override
    public void create(User entity) {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            String query = "insert into Users (firstname, lastname, email) values (?,?,?);";
            PreparedStatement state = connection.prepareStatement(query);
            state.setString(1,entity.getFirstname());
            state.setString(2,entity.getLastname());
            state.setString(3,entity.getEmail());
            state.executeUpdate();
        } catch (SQLException e) {
            LOG.error(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            if (connection != null) {
                connectionPool.returnConnection(connection);
            }
        }
    }

    @Override
    public void update(int id, User entity) {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            String query = "update Users set firstname = ?, lastname = ?, email = ? where id = ?;";
            PreparedStatement state = connection.prepareStatement(query);
            state.setString(1,entity.getFirstname());
            state.setString(2,entity.getLastname());
            state.setString(3,entity.getEmail());
            state.setInt(4,id);
        } catch (SQLException e) {
            LOG.error(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            if (connection != null) {
                connectionPool.returnConnection(connection);
            }
        }
    }

    @Override
    public void delete(int id) {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            String query = "delete from Users where id = ?;";
            PreparedStatement state = connection.prepareStatement(query);
            state.setInt(1,id);
        } catch (SQLException e) {
            LOG.error(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            if (connection != null) {
                connectionPool.returnConnection(connection);
            }
        }
    }

    @Override
    public List<User> finedByFirstname(String parameter) {
        Connection connection = null;
        List<User> results = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            String query = "SELECT id, firstname, lastname, email FROM Users where firstname = ?";
            PreparedStatement state =  connection.prepareStatement(query);
            state.setString(1,parameter);
            ResultSet resultSet = state.executeQuery();
            while (resultSet.next()) {
                results.add(new User(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4)));
            }
        } catch (SQLException e) {
            LOG.error(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            if (connection != null) {
                connectionPool.returnConnection(connection);
            }
        }
        return results;
    }

    @Override
    public List<User> finedByLastname(String parameter) {
        Connection connection = null;
        List<User> results = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            String query = "SELECT id, firstname, lastname, email FROM Users where lastname = ?";
            PreparedStatement state =  connection.prepareStatement(query);
            state.setString(1,parameter);
            ResultSet resultSet = state.executeQuery();
            while (resultSet.next()) {
                results.add(new User(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4)));
            }
        } catch (SQLException e) {
            LOG.error(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            if (connection != null) {
                connectionPool.returnConnection(connection);
            }
        }
        return results;
    }

    @Override
    public List<User> finedByEmail(String parameter) {
        Connection connection = null;
        List<User> results = new ArrayList<>();
        try {
            connection = connectionPool.getConnection();
            String query = "SELECT id, firstname, lastname, email FROM Users where email = ?";
            PreparedStatement state =  connection.prepareStatement(query);
            state.setString(1,parameter);
            ResultSet resultSet = state.executeQuery();
            while (resultSet.next()) {
                results.add(new User(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4)));
            }
        } catch (SQLException e) {
            LOG.error(SQL_ERROR, e);
            throw new RepositoryException(SQL_ERROR, e);
        } finally {
            if (connection != null) {
                connectionPool.returnConnection(connection);
            }
        }
        return results;
    }
}
