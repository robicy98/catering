package edu.bbte.idde.srim1761.backend.dao.jpa;

import javax.persistence.EntityManager;

import edu.bbte.idde.srim1761.backend.dao.Dao;
import edu.bbte.idde.srim1761.backend.model.BaseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public abstract class JPADao<T extends BaseEntity> implements Dao<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(JPADao.class);
    @Autowired
    @Qualifier("jpa")
    private EntityManager entityManager;

    @Override
    public void create(T entity) {
        entityManager.getTransaction().begin();
        entityManager.persist(entity);
        entityManager.getTransaction().commit();
    }

    @Override
    public void update(int id, T entity) {
        entityManager.getTransaction().begin();
        entityManager.merge(entity);
        entityManager.getTransaction().commit();
    }

    @Override
    public void log(String debugMessage) {
        LOGGER.debug(debugMessage);
    }
}
