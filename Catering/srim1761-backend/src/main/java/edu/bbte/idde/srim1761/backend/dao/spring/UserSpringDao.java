package edu.bbte.idde.srim1761.backend.dao.spring;

import edu.bbte.idde.srim1761.backend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface UserSpringDao extends JpaRepository<User, Integer> {

    Collection<User> findByFirstname(String firstname);

    Collection<User> findByLastname(String lastname);

    Collection<User> findByEmail(String email);

}
