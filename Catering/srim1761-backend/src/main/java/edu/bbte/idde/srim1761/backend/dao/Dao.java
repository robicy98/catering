package edu.bbte.idde.srim1761.backend.dao;

import edu.bbte.idde.srim1761.backend.model.BaseEntity;

import java.util.List;

public interface Dao<T extends BaseEntity> {

    List<T> findAll();

    T findById(int id);

    void create(T entity);

    void update(int id, T entity);

    void delete(int id);

    void log(String debuggMessage);
}
