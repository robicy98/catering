package edu.bbte.idde.srim1761.backend.dao;

import edu.bbte.idde.srim1761.backend.model.Menu;

import java.util.List;

public interface MenuDao extends Dao<Menu> {

    List<Menu> findByEloetel(String parameter);

    List<Menu> findByLeves(String parameter);

    List<Menu> findByFoetel(String parameter);

    List<Menu> findByDesszert(String parameter);

    List<Menu> findByItal(String parameter);

}
