package edu.bbte.idde.srim1761.backend.dao;

import edu.bbte.idde.srim1761.backend.model.User;

import java.util.List;

public interface UserDao extends Dao<User> {

    List<User> finedByFirstname(String parameter);

    List<User> finedByLastname(String parameter);

    List<User> finedByEmail(String parameter);

}

