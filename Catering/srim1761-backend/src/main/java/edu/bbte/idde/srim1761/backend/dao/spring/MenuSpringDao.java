package edu.bbte.idde.srim1761.backend.dao.spring;

import edu.bbte.idde.srim1761.backend.model.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface MenuSpringDao extends JpaRepository<Menu, Integer> {

    Collection<Menu> findByEloetem(String eloetel);

    Collection<Menu> findByLeves(String leves);

    Collection<Menu> findByFoetel(String foetel);

    Collection<Menu> findByDesszert(String desszert);

    Collection<Menu> findByItal(String ital);
}
