package edu.bbte.idde.srim1761.backend.dao.mysql;

import edu.bbte.idde.srim1761.backend.dao.Dao;
import edu.bbte.idde.srim1761.backend.model.BaseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class MyDao<T extends BaseEntity> implements Dao<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MyDao.class);

    @Override
    public void log(String debugMessage) {
        LOGGER.debug(debugMessage);
    }
}
