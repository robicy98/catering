package edu.bbte.idde.srim1761.servlet;

import edu.bbte.idde.srim1761.backend.dao.DaoFactory;
import edu.bbte.idde.srim1761.backend.model.Menu;
import edu.bbte.idde.srim1761.backend.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/menus")
public class ViewServlet extends HttpServlet {
    private final DaoFactory database = DaoFactory.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Menu> menus = database.getMenuDao().findAll();
        List<User> users = database.getUserDao().findAll();
        req.setAttribute("data", menus);
        req.setAttribute("user", users);
        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }
}
