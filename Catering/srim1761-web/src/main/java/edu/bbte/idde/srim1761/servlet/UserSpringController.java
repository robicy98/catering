package edu.bbte.idde.srim1761.servlet;

import edu.bbte.idde.srim1761.backend.dao.spring.UserSpringDao;
import edu.bbte.idde.srim1761.backend.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;

@Controller
@RequestMapping("/users")
@Profile("spring")
public class UserSpringController {

    private static final Logger LOG = LoggerFactory.getLogger(MenuSringController.class);

    @Autowired
    private UserSpringDao database;

    @PostConstruct
    public void postConstructor() {
        LOG.info("Server Listening...");
    }

    @GetMapping
    public ResponseEntity getHandle(@RequestParam(value = "id", required = false) String id) {
        return ResponseEntity.ok(database.findAll());
    }

    @PostMapping
    public ResponseEntity postHandle(@RequestBody User body) {
        return null;
    }

    @PutMapping
    public ResponseEntity putHandle(@RequestBody User body) {
        return null;
    }
}
