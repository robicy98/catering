package edu.bbte.idde.srim1761;

import edu.bbte.idde.srim1761.servlet.ApiServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class MainApp extends SpringBootServletInitializer {
    private static final Logger LOG = LoggerFactory.getLogger(ApiServlet.class);

    public static void main(String[] args) {
        LOG.debug("Main started");
        SpringApplication.run(MainApp.class, args);
    }
}
