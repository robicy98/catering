package edu.bbte.idde.srim1761.servlet;

import edu.bbte.idde.srim1761.backend.dao.MenuDao;
import edu.bbte.idde.srim1761.backend.model.Menu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/menus")
@Profile("!spring")
public class ApiServlet {
    private static final Logger LOG = LoggerFactory.getLogger(ApiServlet.class);

    @Autowired
    private MenuDao database;

    private static final String NOT_A_NUMBER = "the given id is not a number format!";

    @GetMapping
    public ResponseEntity doGet(@RequestParam(value = "id", required = false) String id) {

        LOG.info("doGet started");
        if (id == null) {
            List<Menu> menus = database.findAll();
            return ResponseEntity.ok(menus);
        } else {
            if (database.findById(Integer.parseInt(id)) == null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No item with ID: " + id);
            } else {
                try {
                    return ResponseEntity.ok(database.findById(Integer.parseInt(id)));
                } catch (NumberFormatException e) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(NOT_A_NUMBER);
                }
            }
        }
    }

    @PostMapping
    public ResponseEntity postHandle(@RequestBody Menu body) {
        if (body.getEloetel() == null ||
                body.getLeves() == null ||
                body.getFoetel() == null ||
                body.getDesszert() == null ||
                body.getItal() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Data don't match");
        } else {
            database.create(new Menu(body.getEloetel(),
                    body.getLeves(),
                    body.getFoetel(),
                    body.getDesszert(),
                    body.getItal()));
            return ResponseEntity.ok().build();
        }
    }

    @DeleteMapping
    public ResponseEntity deleteHandle(@RequestParam(value = "id", required = true) String id) {
        try {
            if (database.findById(Integer.parseInt(id)) == null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No item with ID: " + id);
            } else {
                return ResponseEntity.ok().build();
            }
        } catch (NumberFormatException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(NOT_A_NUMBER);
        }
    }

    @PutMapping
    public ResponseEntity putHandle(@RequestBody Menu body) {
        if (body.getId() == 0 ||
                body.getEloetel() == null ||
                body.getLeves() == null ||
                body.getFoetel() == null ||
                body.getDesszert() == null ||
                body.getItal() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Data don't match");
        } else {
            database.update(body.getId(),new Menu(body.getEloetel(),
                    body.getLeves(),
                    body.getFoetel(),
                    body.getDesszert(),
                    body.getItal()));
            return ResponseEntity.ok().build();
        }
    }
}