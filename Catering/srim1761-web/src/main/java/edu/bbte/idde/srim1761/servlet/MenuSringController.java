package edu.bbte.idde.srim1761.servlet;

import edu.bbte.idde.srim1761.backend.dao.spring.MenuSpringDao;
import edu.bbte.idde.srim1761.backend.model.Menu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Controller
@RequestMapping("/menus")
@Profile("spring")
public class MenuSringController {
    private static final Logger LOG = LoggerFactory.getLogger(MenuSringController.class);

    @Autowired
    private MenuSpringDao database;

    private static final String NOT_A_NUMBER = "the given id is not a number format!";

    @PostConstruct
    public void postConstructor() {
        LOG.info("Server Listening...");
    }

    @GetMapping
    public ResponseEntity getHandle(@RequestParam(value = "id", required = false) String id) {
        if (id == null) {
            LOG.info("Send all info from database");
            return ResponseEntity.ok(database.findAll());
        } else {
            Optional<Menu> menus = database.findById(Integer.parseInt(id));
            if (menus.isPresent()) {
                try {
                    return ResponseEntity.ok(menus.get());
                } catch (NumberFormatException e) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(NOT_A_NUMBER);
                }
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No item with ID: " + id);
            }
        }
    }

    @PostMapping
    public ResponseEntity postHandle(@RequestBody Menu body) {
        if (body.getEloetel() == null ||
                body.getLeves() == null ||
                body.getFoetel() == null ||
                body.getDesszert() == null ||
                body.getItal() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Data don't match");
        } else {
            database.save(new Menu(body.getEloetel(),
                    body.getLeves(),
                    body.getFoetel(),
                    body.getDesszert(),
                    body.getItal()));
            return ResponseEntity.ok().build();
        }
    }

    @DeleteMapping
    public ResponseEntity deleteHandle(@RequestParam(value = "id", required = true) String id) {
        try {
            if (database.findById(Integer.parseInt(id)).isPresent()) {
                return ResponseEntity.ok().build();
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No item with ID: " + id);
            }
        } catch (NumberFormatException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(NOT_A_NUMBER);
        }
    }

}
