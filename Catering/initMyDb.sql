USE menuk;

CREATE TABLE IF NOT EXISTS`Menu` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`eloetel` VARCHAR(100) NOT NULL,
	`leves` VARCHAR(100) NOT NULL,
	`foetel` VARCHAR(100) NOT NULL,
	`desszert` VARCHAR(100) NOT NULL,
	`ital` VARCHAR(100) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `Users` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`firstname` VARCHAR(100) NOT NULL DEFAULT '',
	`lastname` VARCHAR(100) NOT NULL DEFAULT '',
	`email` VARCHAR(100) NOT NULL DEFAULT '',
	PRIMARY KEY (`id`)
);

insert into Menu (eloetel, leves, foetel, desszert, ital) values ("Salata","pacal","ciganypecsenye","linzer","friza");
insert into Menu (eloetel, leves, foetel, desszert, ital) values ("Hideg Tal","Parasztcsorba","ratott hus korettel","dobos","Carassia");
insert into Menu (eloetel, leves, foetel, desszert, ital) values ("Cezar salata","Pacal","Csulok babbal","ecler","Vinca");
insert into Menu (eloetel, leves, foetel, desszert, ital) values ("Kaviar","Szarvasgomba kremleves","Gombhal","Navacake","Dom Pérignon 2004 Brut in Magnum");

